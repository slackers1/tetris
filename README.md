How to run the game:
1. tetriproject.web.app
2. tetriproject.firebaseapp.com
3. open the ./public/index.html in brower

How to play:
1. There are three modes in the game: single player, player vs player, online play
2. For single player, just use left and right arrow keys to move, up arrow key to rotate, down arrow key to falling fast
3. The player vs player mode allows two players to play on one computer. One player uses the arrow keys to control, another uses WASD to control
4. The online play mode allows the player to play with others online. Click on "online play" and wait for other people to join in. Use arrow key to control.
