var count = 0;
// For Firebase JS SDK v7.20.0 and later, measurementId is optional

const firebaseConfig = {
    apiKey: "AIzaSyA2jrMj-xYZUf1EDjyhx3L3usUyMsFG5SM",
    authDomain: "tetriproject.firebaseapp.com",
    databaseURL: "https://tetriproject.firebaseio.com",
    projectId: "tetriproject",
    storageBucket: "tetriproject.appspot.com",
    messagingSenderId: "930533909987",
    appId: "1:930533909987:web:eac8788783494a334becd6",
    measurementId: "G-034ZFHH0JM"
};


firebase.initializeApp(firebaseConfig);

var writeUserData = function (roomId, userId, board) {
    firebase.database().ref(roomId + '/' + userId).set({
        name: userId,
        gameBoard: board
    });
}


var readUserData = function (roomId, userId) {
    firebase.database().ref(roomId + '/' + userId).once('value').then(function (snapshot) {
        let gameBoard = snapshot.val() && snapshot.val().gameBoard || 'no info';
        console.log("here is the: ", gameBoard);
        //console.log(snapshot);
    });
}

var numUsers = function (roomId) {
    firebase.database().ref(roomId).once('value').then(function (snapshot) {
        let num = snapshot.numChildren();
        console.log(num);
    });
}


function create_UUID(){
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (dt + Math.random()*16)%16 | 0;
        dt = Math.floor(dt/16);
        return (c=='x' ? r :(r&0x3|0x8)).toString(16);
    });
    return uuid;
}

function join_room(roomId, userId){
  console.log("join in room: current count=" + count);
  if (count >= 2){
    console.log("room is full");
    return false;
   }
  firebase.database().ref(roomId).once("value").then(function (snapshot){
    console.log("join ffunction");
    if (snapshot.val() && !snapshot.child(userId).val()){  // if the user is not in the room already
      console.log("not in the room, join in");
      writeUserData("room1", userId, null);
    }else if (!snapshot.val()){ // if there is no room
      console.log("create a room, join in");
      writeUserData("room1", userId, null);
    }else{
      console.log("you are in the room");
    }
    
  });
  return true;
}

function leave_room(roomId, userId){
  firebase.database().ref(roomId + "/" + userId).remove();
}






$(function(){
// let myUserId = create_UUID();
// console.log("my id is: " + myUserId);

// $("#btn1").click(function(){
//   alert("join");
//   join_room("room1", myUserId);
// });

// $("#btn2").click(function(){
//   alert("leave");
//   leave_room("room1", myUserId);
// });

// callback function when room1 changed
firebase.database().ref("room1").on("value", function(ss){
// console.log("added", ss.val());
  count = ss.numChildren();
  console.log("current count", count);
  
  // let player = "";
  // ss.forEach(function(child_ss){
  //   player += ss.val() && child_ss.val().name || "no player";
  //   player += ", ";
  // });
  // $("#members").text(player);
});

});
