var isInGame = false;
var tetrisGame = new TetrisGame("singleGame");
var twoGame0 = new TetrisGame("twoGame0");
var twoGame1 = new TetrisGame("twoGame1");
var audioOn = true;
$(function () {
  $('#game-over').hide();
  $('#win-game').hide();
  $("#singlePlayer").hide();
  $("#twoPlayer").hide();
  

  // menu selection show cute image in front of button

  $("#singlebtn").click(function () {
    $("#menuScreen").fadeOut("slow", function () {
      $(".blockImg").each(function (index) {
        $(this).css({
          top: index * 18 + 6 + "%",
          transform: "translateX(-50%)",
          left: "50%",
        });
      });
      $("#singlePlayer").fadeIn("slow", function () {
        tetrisGame.startGame();
        isInGame = true;
        $("#aaaaaaaa")[0].play();
      });
    });
  });

  $("#twoplayerbtn").click(function () {
    $("#menuScreen").fadeOut("slow", function () {
      $(".blockImg").each(function (index) {
        $(this).css({
          top: index * 20 - 100 + 5 + "%",
          transform: "translateX(-50%)",
          left: "50%",
        });
      });
      $("#twoPlayer").fadeIn("slow", function () {
        twoGame0.startGame();
        twoGame1.startGame();
        $("#aaaaaaaa")[0].play();
      });
    });
  });

  $("#onlinePlaybtn").click(function () {
    let myId = create_UUID();
    if (join_room("room1", myId)){
      $("#menuScreen").fadeOut("slow", function () {
        $(".blockImg").each(function (index) {
          $(this).css({
            top: index * 20 - 100 + 5 + "%",
            transform: "translateX(-50%)",
            left: "50%",
          });
        });
        $("#twoPlayer").fadeIn("slow", function () {
          let og = new OnlineGame("twoGame1", myId);
          og.startGame();
          $("#aaaaaaaa")[0].play();
          // when refresh or close page
          window.onbeforeunload = function () {
            leave_room("room1", og.myId);
            //return 'Are you really want to perform the action?';
          }
        });
      });
    }else {
      alert("room is currently full! number of play in room =" + count);
    }
  });

  // drawing game borad
  var singlePlayerWidth = $("#singlePlayer").width();
  var singlePlayerHeight = $("#singlePlayer").height();
  const blockW = (singlePlayerWidth * 0.6) / GAME_WIDTH;
  const blockH = singlePlayerHeight / GAME_HEIGHT;
  const dblockW = (singlePlayerWidth * 0.45) / GAME_WIDTH;
  const dblockH = singlePlayerHeight / GAME_HEIGHT;
  for (var i = 0; i < GAME_HEIGHT; i++) {
    for (var j = 0; j < GAME_WIDTH; j++) {
      $(`<div id='block${i}${j}'></div>`)
        .css({
          position: "absolute",
          width: blockW,
          height: blockH,
          top: blockH * i,
          left: blockW * j,
          border: "solid 0.5px white",
          "background-color": "black",
        })
        .appendTo("#singleGameScene");

      $(`<div id='two0block${i}${j}'></div>`)
        .css({
          position: "absolute",
          width: dblockW,
          height: dblockH,
          top: dblockH * i,
          left: dblockW * j,
          border: "solid 0.5px white",
          "background-color": "black",
        })
        .appendTo("#twoGameScene0");
      $(`<div id='two1block${i}${j}'></div>`)
        .css({
          position: "absolute",
          width: dblockW,
          height: dblockH,
          top: dblockH * i,
          left: dblockW * j,
          border: "solid 0.5px white",
          "background-color": "black",
        })
        .appendTo("#twoGameScene1");
    }
  }

  $(".menu-btn button").each(function () {
    $(this)
      .mouseenter(function () {
        $(this).prepend(
          $("<img >", {
            id: "cubeLogo",
            src: "static/images/cube.svg",
            style: "width:10%; height:10% position: absolute; left: 0",
          })
        );
        var myadido = document.getElementById("dddddddd");
        myadido.play();
      })
      .mouseleave(function () {
        $("#cubeLogo").remove();
      });
  });
  //audio mute
  $(".audio-icon").click(function (){
    audioOn = !audioOn;
    $("#aaaaaaaa").prop("muted", !audioOn);
    $("#bbbbbbbb").prop("muted", !audioOn);
    $("#cccccccc").prop("muted", !audioOn);
    $("#dddddddd").prop("muted", !audioOn);
    $("#eeeeeeee").prop("muted", !audioOn);
    $("#ffffffff").prop("muted", !audioOn);
    if(!audioOn){
      $("#wave1").hide();
      $("#wave2").hide();
      $("#wave3").hide();
    }
    else{
      $("#wave1").show();
      $("#wave2").show();
      $("#wave3").show();  
    }
  });


  // update game object
});
