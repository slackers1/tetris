const GAME_HEIGHT = 21;
const GAME_WIDTH = 18; // 4 x 4
const WALL = 9;
const zeroRow = [20, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20, 20];
const levelScore = [0,30,100, 300, 500, 750];

const keyMap = {
  singleGame: [40, 37, 39, 38],
  twoGame1: [40, 37, 39, 38],
};
class TetrisGame {
  constructor(gameType) {
    console.log("tetrisGame");
    let self = this;
    this.basicShape = [
      [
        [0, 1, 0, 0],
        [0, 1, 1, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 0],
      ],
      [
        [0, 0, 0, 0],
        [0, 2, 2, 0],
        [0, 2, 2, 0],
        [0, 0, 0, 0],
      ],
      [
        [0, 0, 0, 0],
        [4, 4, 4, 0],
        [0, 0, 4, 0],
        [0, 0, 0, 0],
      ],
      [
        [0, 0, 0, 0],
        [3, 3, 3, 3],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
      ],
      [
        [0, 0, 0, 0],
        [0, 5, 5, 5],
        [0, 0, 5, 0],
        [0, 0, 0, 0],
      ],
      [
        [0, 0, 6, 0],
        [0, 6, 6, 0],
        [0, 6, 0, 0],
        [0, 0, 0, 0],
      ],
      [
        [0, 0, 0, 0],
        [0, 7, 7, 7],
        [0, 7, 0, 0],
        [0, 0, 0, 0],
      ],
    ];

    this.gameBoard = [];
    this.currentPosition = [];
    this.currentShape = [];
    this.nextShape = this.getBasicShape();
    this.gameSpeed = 500;
    this.timer = 0;
    this.level = 1;
    this.score = 0;
    this.lose = false;
    this.gameType = gameType;
  }

  buildGameboard() {
    console.log("buildBoard");
    let board = [];
    for (let i = 0; i < GAME_HEIGHT; i++) {
      board[i] = [];
      for (let j = 0; j < GAME_WIDTH; j++) {
        board[i][j] = 0;
      }
    }
    return board;
  }

  startGame() {
    let self = this;
    self.gameBoard = self.buildGameboard();
    //console.log(this.gameBoard);
    self.currentShape = self.getBasicShape();
    self.currentPosition = [0, 6];
    //moveShape(currentPosition, [1,0], currentShape);
    //updateBoard(currentPosition, currentShape);

    // setUp wall
    for (let i = 0; i < GAME_WIDTH; i++) {
      self.gameBoard[GAME_HEIGHT - 1][i] = WALL;
      self.gameBoard[GAME_HEIGHT - 2][i] = WALL;
      // self.gameBoard[GAME_HEIGHT - 3][i] = -2;
      // self.gameBoard[GAME_HEIGHT - 4][i] = -3;
      // self.gameBoard[GAME_HEIGHT - 5][i] = -3;
      // self.gameBoard[GAME_HEIGHT - 6][i] = -3;
    }
    // self.gameBoard[GAME_HEIGHT - 3][5] = 0;
    // self.gameBoard[GAME_HEIGHT - 4][5] = 0;
    // self.gameBoard[GAME_HEIGHT - 5][5] = 0;
    // self.gameBoard[GAME_HEIGHT - 6][5] = 0;
    for (let i = 0; i < GAME_HEIGHT; i++) {
      self.gameBoard[i][0] = WALL;
      self.gameBoard[i][1] = WALL;
      self.gameBoard[i][GAME_WIDTH - 1] = WALL;
      self.gameBoard[i][GAME_WIDTH - 2] = WALL;
    }

    window.onkeyup = function (e) {
      if (e.keyCode === keyMap[self.gameType][0]) {
        $("#cccccccc")[0].play();
        //down
        while (self.moveShape([1, 0]));
        self.updateBoard();
      }
      if (e.keyCode === 83) {
        $("#cccccccc")[0].play();
        //down
        while (twoGame0.moveShape([1, 0]));
        twoGame0.updateBoard();
      }


      if (e.keyCode === keyMap[self.gameType][1]) {
        //left
        self.moveShape([0, -1]);
        self.updateBoard();
      }
      if (e.keyCode === 65) {
        //left
        twoGame0.moveShape([0, -1]);
        twoGame0.updateBoard();
      }


      if (e.keyCode === keyMap[self.gameType][2]) {
        //right
        self.moveShape([0, +1]);
        self.updateBoard();
      }
      if (e.keyCode === 68) {
        //right
        twoGame0.moveShape([0, +1]);
        twoGame0.updateBoard();
      }


      if (e.keyCode === keyMap[self.gameType][3]) {
        // up
        self.rotateShape(self.currentShape);
        //console.log(self.currentShape);
        self.updateBoard();
      }
      if (e.keyCode === 87) {
        // up
        twoGame0.rotateShape(twoGame0.currentShape);
        //console.log(self.currentShape);
        twoGame0.updateBoard();
      }
    };

    // update
    this.setUpGameInterval();

    this.collisionTimer = setInterval(function () {
      self.timer += 1;
      //console.log(self.timer);
    }, 100);
  }
  
  setUpGameInterval(){
    let self = this;
    this.gameInterval = setInterval(function () {
      self.moveShape([1, 0]);
      self.updateBoard();
      //self.updateBoard();
    }, self.gameSpeed);

  }

  updateBoard() {
    let self = this;
    if (self.lose){
      console.log("lose");
      return;
    }
    let shapeRowIndex = 3;
    let shapeColIndex = 0;
    for (let i = GAME_HEIGHT - 1; i >= 0; i--) {
      for (let j = 0; j < GAME_WIDTH; j++) {
        if (
          i <= self.currentPosition[0] &&
          i >= self.currentPosition[0] - 3 &&
          j >= self.currentPosition[1] &&
          j <= self.currentPosition[1] + 3
        ) {
          if (self.gameBoard[i][j] < WALL && self.gameBoard[i][j] >= 0)
            // in case 0 in basicShape override other things

            self.gameBoard[i][j] =
              self.currentShape[shapeRowIndex][shapeColIndex];
          //console.log(shapeRowIndex,shapeColIndex ,currentShape[shapeRowIndex][shapeColIndex]);
          shapeColIndex = (shapeColIndex + 1) % 4;
          if (shapeColIndex == 0) {
            shapeRowIndex = shapeRowIndex - 1;
          }
        } else if (self.gameBoard[i][j] < WALL && self.gameBoard[i][j] > 0) {
          self.gameBoard[i][j] = 0;
        }
      }
    }

    // update score
    self.updateLevel()
    
    // upload gameBoard to database
    //self.uploadBoard();


    // draw
    self.draw();

    
  }

  draw(){
    let self = this;
    $("#currentScore").text(self.score);
    $("#currentLevel").text(self.level);
    //console.log(self.gameBoard);
    //console.log(self.gameBoard);
    var singlePlayerWidth = $("#singlePlayer").width();
    var singlePlayerHeight = $("#singlePlayer").height();
    const blockW = (singlePlayerWidth * 0.6) / (GAME_WIDTH - 2);
    const blockH = singlePlayerHeight / (GAME_HEIGHT - 1);

    const dblockW = (singlePlayerWidth * 0.425) / (GAME_WIDTH - 2);
    const dblockH = singlePlayerHeight / (GAME_HEIGHT - 1);

    if (self.gameType === "singleGame") $("#singleGameScene").empty();
    else if (self.gameType === "twoGame0") $("#twoGameScene0").empty();
    else if (self.gameType === "twoGame1") $("#twoGameScene1").empty();
    for (var i = 0; i < GAME_HEIGHT - 1; i++) {
      for (var j = 1; j < GAME_WIDTH - 1; j++) {
        var color = () => {
          if (Math.abs(self.gameBoard[i][j]) === 1) return "orange";
          else if (Math.abs(self.gameBoard[i][j]) === 2) return "yellow";
          else if (Math.abs(self.gameBoard[i][j]) === 3) return "blue";
          else if (Math.abs(self.gameBoard[i][j]) === 4) return "purple";
          else if (Math.abs(self.gameBoard[i][j]) === 5) return "pink";
          else if (Math.abs(self.gameBoard[i][j]) === 6) return "#E0FFFF";
          else if (self.gameBoard[i][j] === 0) return "black";
          else if (self.gameBoard[i][j] === WALL) return "red";
          else {
            return "green";
          }
        };
        if (self.gameType === "singleGame") {
          $(`<div id='block${i}${j}'></div>`)
            .css({
              position: "absolute",
              width: blockW,
              height: blockH,
              top: blockH * i,
              left: blockW * (j - 1),
              border: "solid 0.3px #AAAAAA",
              "background-color": color,
            }).addClass('blockblock')
            .appendTo("#singleGameScene");
        } else if (self.gameType === "twoGame0") {
          $(`<div id='0block${i}${j}'></div>`)
            .css({
              position: "absolute",
              width: dblockW,
              height: dblockH,
              top: dblockH * i,
              left: dblockW * (j - 1),
              border: "solid 0.3px #AAAAAA",
              "background-color": color,
            }).addClass('blockblock')
            .appendTo("#twoGameScene0");
        } else {
          $(`<div id='1block${i}${j}'></div>`)
            .css({
              position: "absolute",
              width: dblockW,
              height: dblockH,
              top: dblockH * i,
              left: dblockW * (j - 1),
              border: "solid 0.3px #AAAAAA",
              "background-color": color,
            }).addClass('blockblock')
            .appendTo("#twoGameScene1");
        }
      }
    }

    //console.log(111);
    $("#nextPreview").empty();
    for (var i = 0; i < 4; i++) {
      for (var j = 0; j < 4; j++) {
        var color = () => {
          if (Math.abs(self.nextShape[i][j]) === 1) return "orange";
          else if (Math.abs(self.nextShape[i][j]) === 2) return "yellow";
          else if (Math.abs(self.nextShape[i][j]) === 3) return "blue";
          else if (Math.abs(self.nextShape[i][j]) === 4) return "purple";
          else if (Math.abs(self.nextShape[i][j]) === 5) return "pink";
          else if (Math.abs(self.nextShape[i][j]) === 6) return "#E0FFFF";
          else if (self.nextShape[i][j] === 0) return "transparent";
          else return "green";
        };
        $(`<div id='preview${i}${j} class='abcdeee'></div>`)
          .css({
            position: "absolute",
            width: blockW,
            height: blockH,
            top: blockH * i,
            left: blockW * j,
            transform: `translateY(${blockH}px) translateX(${blockH}px)`,
            border: "solid 0.5px rgb(207, 199, 120)",
            "background-color": color,
          }).addClass('blockblock')
          .appendTo("#nextPreview");
      }
    }
  }
  moveShape(direction) {
    let self = this;
    let newPosition = [];
    newPosition[0] = self.currentPosition[0] + direction[0];
    newPosition[1] = self.currentPosition[1] + direction[1];

    if (newPosition[0] < 0 || newPosition[0] >= GAME_HEIGHT) {
      // newPosition go out of bound;
      //console.log("Warnning: Out of Height Bound!!!!!!!!!!!!!!!");
      this.collisionTimerControl();
      return false;
    }
    if (newPosition[1] < 0 || newPosition[1] >= GAME_WIDTH) {
      //console.log("Warnning: Out of Width Bound!!!!!!!!!!!!!!!");
      return false;
    }

    if (self.checkCollision(newPosition, self.currentShape)) {
      //move(newPosition, currentShape)
      self.currentPosition[0] = newPosition[0];
      self.currentPosition[1] = newPosition[1];
      if (direction[0] == 1) {
        // if move downward is valid, reset collision timer
        console.log("reset");
        self.timer = 0;
      }
      return true;
    } else {
      //console.log("collide !!!!!!!!!!!!!!!");
      return false;
    }
  }

  // call the function to rotate shape 90 degree clockwise
  rotateShape(shape) {
    $("#bbbbbbbb")[0].play();
    let self = this;
    let newShape = [
      [0, 0, 0, 0],
      [0, 0, 0, 0],
      [0, 0, 0, 0],
      [0, 0, 0, 0],
    ];
    for (let i = 0; i < shape.length; i++) {
      for (let j = 0; j < shape[i].length; j++) {
        newShape[j][shape.length - 1 - i] = shape[i][j];
      }
    }
    if (self.checkCollision(self.currentPosition, newShape)) {
      //console.log("rotate");
      self.currentShape = newShape;
    } else {
      //console.log("collide !!!!!!!!!!!!!!");
      //return shape;
    }
  }

  checkCollision(position, shape) {
    let self = this;
    let shapeRowIndex = 3;
    let shapeColIndex = 0;
    let curBoard = self.gameBoard;
    for (let i = position[0]; i >= 0 && i >= position[0] - 3; i--) {
      shapeColIndex = 0;
      for (let j = position[1]; j < GAME_WIDTH && j <= position[1] + 3; j++) {
        //console.log(i,j,": ", curBoard[i][j], "; ", shapeRowIndex, shapeColIndex,", " ,shape[shapeRowIndex][shapeColIndex]);
        if (curBoard[i][j] == WALL &&  shape[shapeRowIndex][shapeColIndex] > 0) {   //c + s > Wall
          self.collisionTimerControl();
          return false;
        }
        if (curBoard[i][j] < 0 && shape[shapeRowIndex][shapeColIndex] > 0) {
          self.collisionTimerControl();
          return false;
        }
        //curBoard[i][j] = currentShape[shapeRowIndex][shapeColIndex];
        shapeColIndex += 1;
      }
      shapeRowIndex -= 1;
    }
    return true;
  }

  collisionTimerControl() {

    let self = this;
    if (self.timer >= 6) {
      // no action take after collision
      for (let i = 0; i < 4; i++) {
        for (let j = 0; j < 4; j++) {
          self.currentShape[i][j] = 0 - self.currentShape[i][j];
        }
      }
      self.updateBoard();
      var isCleared = self.clearRow();
      if (isCleared[GAME_HEIGHT] === 1) {
        $("#eeeeeeee")[0].play();
        self.score += 15;
        self.moveDown(isCleared);
      } else if (isCleared[GAME_HEIGHT] > 1) {
        self.score += 20 * isCleared[GAME_HEIGHT];
        $("#ffffffff")[0].play();
        self.moveDown(isCleared);
      }

      //console.log("here", self.gameBoard);

      // when collide the currentPostion is at begining
      if (self.currentPosition[0] === 0){
        self.loseGame();
        return;
      }
      self.currentPosition = [0, 6];

      // reset currenShape
      self.currentShape = self.nextShape;
      self.nextShape = this.getBasicShape();
      
      
      self.timer = 0;
    }
  }

  moveDown(nums) {
    var self = this;
    for (var i = GAME_HEIGHT - 1; i >= 0; i--) {
      for (var j = 0; j < GAME_WIDTH; j++) {
        self.gameBoard[i + nums[i]][j] = self.gameBoard[i][j];
      }
      if (i < GAME_HEIGHT - 2) {
        for (var k = 0; k < GAME_WIDTH; k++) {
          //self.gameBoard[i][k] = zeroRow[k];
        }
      }
    }
  }

  clearRow() {
    let self = this;
    let clear = false;

    let tmpCount = 0;
    let moveCount = [
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
    ];
    for (let i = GAME_HEIGHT - 3; i >= 0; i--) {
      var count = 0;
      for (let j = 0; j < GAME_WIDTH; j++) {
        if (self.gameBoard[i][j] === 0) count++;
      }
      if (count === 0) {
        moveCount[i] = 0;
        moveCount[21]++;
        clear = true;
        for (var k = 0; k < GAME_WIDTH; k++) {
          self.gameBoard[i][k] = zeroRow[k];
        }
        tmpCount++;
      } else {
        moveCount[i] = tmpCount;
      }
    }

    return moveCount;
  }

  loseGame(){
    let self = this;
    self.lose = true;
    clearInterval(self.gameInterval);
    clearInterval(self.collisionTimer);
    console.log("cancle interval");
    self.endGame();
  }

  updateLevel(){
    let self = this;
    //console.log(self.score, self.level);
    if (self.score >= levelScore[parseInt(self.level)]){
      
      self.level += 1;
      self.gameSpeed = self.gameSpeed - (self.level-1) * 50;
      //console.log(self.gameSpeed);
      clearInterval(self.gameInterval);
      this.gameInterval = setInterval(function () {
        self.moveShape([1, 0]);
        self.updateBoard();
      }, self.gameSpeed);
    }
    
  }

  endGame() {
    $("#game-over").fadeIn("fast");

    window.onkeyup = function (e) {
      if (e.keyCode != null) {
        $("#game-over").fadeOut("fast", function () {
          $("#singlePlayer").hide();
          $("#twoPlayer").hide();
          $("#game-over").hide();
          $("#menuScreen").fadeIn("slow"); 
          $("#singleGameScene").empty();
          $("#twoGameScene0").empty();
          $("#twoGameScene1").empty();
          tetrisGame = new TetrisGame("singleGame");
          twoGame0 = new TetrisGame("twoGame0");
          twoGame1 = new TetrisGame("twoGame1");
        });
      }
    };
  }

  getBasicShape() {
    let self = this;
    let theShape = //self.basicShape[3];
      self.basicShape[Math.floor(Math.random() * self.basicShape.length)];
    let result = [];
    for (let i = 0; i < 4; i++) {
      result[i] = [];
      for (let j = 0; j < 4; j++) {
        result[i][j] = theShape[i][j];
      }
    }
    return result;
  }

  uploadBoard(){
    // this is Single player game, do not need to upload board to database
    // override this in online game
  }
}
