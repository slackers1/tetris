class OnlineGame extends TetrisGame{
    constructor(gameType, myId){
        console.log("call super constructor");
        super(gameType);
        this.startFlag = false;
        this.myId = myId;
        console.log("my Id is:" + this.myId);
        this.opponentBoard = "no board";
        this.opponentId = "no id";
        this.gameFlag = false;
        this.win = false;
    }
    

    // override
    startGame() {
        super.startGame();
        
        
        let self = this;
        //join_room("room1", self.myId);
        self.onOpponentChanged("room1");
        
    }

    // override
    setUpGameInterval(){
        let self = this;
        this.gameInterval = setInterval(function () {
            if (count == 2){
                self.gameFlag = true;
                self.moveShape([1, 0]);
                self.updateBoard();
                //self.updateBoard();
                self.uploadBoard();
            }else{
                self.timer = 0;
                if (self.gameFlag){
                    self.win = true;
                    self.endGame();
                }else {
                    console.log("Warning: not enough Player in the room, number of player: " + count);
                }
            }
        }, self.gameSpeed);
    }

    // override: upload the local board; load other player's board
    uploadBoard(){
        if (this.lose){
            return;
        }
        let self = this;
        //console.log("online game, upload board");
        // upload my board
        let boardStr = self.convertBoardToString();
        writeUserData("room1", self.myId, boardStr);

        //read opponent's board
        //console.log("opponent: " + self.opponentId);
        // firebase.database().ref("room1" + '/' + self.opponentId).once('value').then(function (snapshot) {
        //     if (snapshot.val() && snapshot.val().gameBoard){
        //         self.convertStringToBoard(snapshot.val().gameBoard);
        //     }
        // });
        
    }
    
    // override
    draw(){
        super.draw();
    }

    // callback function to drag opponent when it's borad change
    onOpponentChanged(roomId){
        let self = this;
        self.findSiblingKey(roomId);

        firebase.database().ref(roomId).on("value", function(ss){
            ss.forEach(function(child){
                if (child.key != self.myId && child.val().gameBoard){
                    self.convertStringToBoard(child.val().gameBoard);
                }
            });

            
        });
    }


    drawOpponent(){
        console.log("draw opponent");
        $("#singleGameScene").empty();
        $("#twoGameScene0").empty();
        let self =this;
        const dblockW = ($("#singlePlayer").width() * 0.425) / (GAME_WIDTH - 2);
        const dblockH = $("#singlePlayer").height() / (GAME_HEIGHT - 1);
        for (var i = 0; i < GAME_HEIGHT - 1; i++) {
            for (var j = 1; j < GAME_WIDTH - 1; j++) {
              var color = () => {
                  //console.log("!!!!!!!!!!!!!!!!!!!!!!",self.opponentBoard);
                if (Math.abs(self.opponentBoard[i][j]) === 1) return "orange";
                else if (Math.abs(self.opponentBoard[i][j]) === 2) return "yellow";
                else if (Math.abs(self.opponentBoard[i][j]) === 3) return "blue";
                else if (Math.abs(self.opponentBoard[i][j]) === 4) return "purple";
                else if (Math.abs(self.opponentBoard[i][j]) === 5) return "pink";
                else if (Math.abs(self.opponentBoard[i][j]) === 6) return "#E0FFFF";
                else if (self.opponentBoard[i][j] === 0) return "black";
                else if (self.opponentBoard[i][j] === WALL) return "red";
                else {
                  return "green";
                }
              };
               
                $(`<div id='0block${i}${j}'></div>`)
                  .css({
                    position: "absolute",
                    width: dblockW,
                    height: dblockH,
                    top: dblockH * i,
                    left: dblockW * (j - 1),
                    border: "solid 0.3px #AAAAAA",
                    "background-color": color,
                  }).addClass('blockblock')
                  .appendTo("#twoGameScene0");
            }
        }
    }

    convertBoardToString(){
        let self = this;
        let boardStr = "";
        for (let i = 0; i < GAME_HEIGHT; i++){
                boardStr += (self.gameBoard[i].join() + ".");
        }
        return boardStr.substr(0, boardStr.length - 1)
    }

    convertStringToBoard(boardStr){
        let self = this;
        this.opponentBoard = [];
        let index = 0;      
        boardStr.split(".").map((val, indexRow) => {
            this.opponentBoard.push([]);
            val.split(",").map((val2) => {
                this.opponentBoard[indexRow].push( parseInt(val2));
            });
        });
        //console.log(this.opponentBoard);
        if (this.opponentBoard.length !== 0){
            self.drawOpponent();
        }
    }

    

    findSiblingKey(roomId){
        let self = this;
        firebase.database().ref(roomId).on("value", function(ss){
          ss.forEach(function(child){
            if(child.key != self.myId){
              self.opponentId = child.key;
              //console.log("sibling key is: " + siblingKey);
            }
          });
        });
      }

      endGame(){
        let self = this;
        clearInterval(self.gameInterval);
        clearInterval(self.collisionTimer);
        leave_room("room1", self.myId);
        let myStr = "";
        if (self.win){
            //alert("you win");
            myStr = "#win-game";
            //$("#win-game").fadeIn("fast");
        }
        if (self.lose){
            //alert("you lose");
            //$("#game-over").fadeIn("fast");
            myStr = "#game-over";
        }
        
        $(myStr).fadeIn("fast");
        window.onkeyup = function (e) {
            if (e.keyCode != null) {
              $(myStr).fadeOut("fast", function () {
                $("#singlePlayer").hide();
                $("#twoPlayer").hide();
                $("#game-over").hide();
                $("#menuScreen").fadeIn("slow"); 
                $("#singleGameScene").empty();
                $("#twoGameScene0").empty();
                $("#twoGameScene1").empty();
                tetrisGame = new TetrisGame("singleGame");
                twoGame0 = new TetrisGame("twoGame0");
                twoGame1 = new TetrisGame("twoGame1");
              });
            }
          };
      }
    

}